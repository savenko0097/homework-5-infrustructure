## Задание

Сверстать макет [Sk Infrustructure](https://www.figma.com/file/1GyF5Kz5tgKuQIEK9Luhou/HW---SK-Infrustructure?node-id=0%3A1).

#### Технические требования к верстке

- Кнопка обратный звонок должна быть сделана в виде ссылки
- Элементы в первой секции `Наружные тепловые сети` должна быть выполнена с помощью CSS Grid.
    

Для удобства, полные версии всех фоновых картинок вынесены в отдельную [папку](img) и даются вместе с макетом.

#### Необязательное задание продвинутой сложности

- Фоновые картинки должны быть зафиксированы на заднем фоне, и не двигаться при скролле, но между разными блоками должны быть показаны разные картинки, пример работы данного свойства можно увидеть [здесь](./Preview.gif).
- Сверстать вторую версию этого макета, в котором фоновые картинки между блоками при скроллинге будут двигаться медленнее, чем основные блоки с текстом, но между разными блоками по-прежнему должны быть разные картинки.

#### Примечание
- Верстка должна быть выполнена без использования CSS библиотек типа Bootstrap или Materialize.

#### Полезные ссылки

[CSS FlexBox](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson11_flexbox/flexbox.html)

[CSS Grid](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson12_grid/grid.html)

[Parallax](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson13_animation_parallax/parallax.html)
